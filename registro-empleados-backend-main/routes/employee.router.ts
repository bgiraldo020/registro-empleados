import { Router, Request, Response } from 'express';
import { Employee } from '../db/db-config';

import EmployeeController from '../controllers/employee.controller';

const router = Router();
const employeeController = EmployeeController.instance;

router.get('/', (req: Request, res: Response) => {
    res.status(201).json({
        ok: true,
        msg: 'GET - SUCCESS'
    });
});

// Ruta para crear empleado
router.post('/create-employee', employeeController.createEmployee);

// Ruta para obtener los empleados
router.get('/list-employees', employeeController.listEmployee);

// Ruta para eliminar empleado
router.delete('/delete-employee/:idType/:idNumber', employeeController.deleteEmployee);

// Ruta para obtener un empleado
router.get('/one-employee/:idType/:idNumber', employeeController.oneEmployee);

// Ruta para obtener un empleado por correo
router.get('/check-email/:email/:idType/:idNumber', employeeController.oneEmployeeForEmail);

// Ruta para editar un empleado
router.put('/edit-employee/:idType/:idNumber', employeeController.editEmployee);

export default router;