// Se importa la liberia para gestion de las sentencias SQL
import { Sequelize } from 'sequelize';
// Importo el modelo de empleados
import { EmployeeFactory } from '../models/employee.model';

// Genero un instancia para la configuracion de la base de datos
export const dbConfig = new Sequelize(
    {
        username: 'root',                   // Usuario de la base de datos
        password: '',                       // Contraseña de la base de datos
        database: 'employees_cidenet',      // Nombre asignado de la base de datos
        host: 'localhost',                  // Host de la base de datos
        dialect: 'mysql',                   // Motor de la base de datos
        port: 3306,                         // Puerto asignado de la base de datos
        logging: false,                     // Bandera para mostrar la sentencia en consola
    }
);

// Se genera la tabla empleados
export const Employee = EmployeeFactory(dbConfig);