import Server from './classes/server';
import { dbConfig } from './db/db-config';
import routerEmployee from './routes/employee.router';

// Intancia de la clase Server
const server = Server.instance;

// Rutas de empleados
server.app.use('/employee', routerEmployee);

// Inicio de servidor
server.start(() => {
    console.log(`Server listo en el puerto: ${server.port} \x1b[32m%s\x1b[0m`, 'online');
    // Conexion de base de datos
    dbConfig
        .sync().then(() => {
            console.log('Base de datos: \x1b[32m%s\x1b[0m', 'online');
        })
        .catch((error: any) => {
            throw error;
        })
});