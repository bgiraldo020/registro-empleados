// Dominios de correo
export const DOMAIN = ['', 'cidenet.com.co', 'cidenet.com.us'];

// Servidor REST, en este documento cambiar el puerto segun el
// Puerto asignado en el BackEnd
export const URL_SERVICES = 'http://localhost:3030/';
