// Modelo de los filtros
export interface FilterSearchModel {
    input: string;
    idType: string;
    country: string;
    state: string;
}