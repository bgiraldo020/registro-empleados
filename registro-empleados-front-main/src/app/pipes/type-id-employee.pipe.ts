import { Pipe, PipeTransform } from '@angular/core';
import { SuppliesUtils } from '../utils/supplies.utils';

@Pipe({
    name: 'typeIdEmployee'
})
export class TypeIdEmployeePipe implements PipeTransform {

    // Pipe para mostrar en texto la opcion de tipo de identificacion seleccionada en la tabla
    transform(value: number): string {

        if (value > 0) {
            return SuppliesUtils.TYPE_ID_EMPLOYEES[value - 1];
        }

        return '';
    }

}
