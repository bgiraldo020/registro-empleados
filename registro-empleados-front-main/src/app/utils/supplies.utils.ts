export class SuppliesUtils {
    // Estado
    static STATES_EMPLOYEES = ['Activo', 'Inactivo'];

    // Areas de trabajo
    static AREAS_EMPLOYEES = [
        'Administración',
        'Financiera',
        'Compras',
        'Infraestructura',
        'Operación',
        'Talento Humano',
        'Servicios Varios'
    ];

    // Tipo de identificacion
    static TYPE_ID_EMPLOYEES = [
        'Cédula de Ciudadanía',
        'Cédula de Extranjería',
        'Pasaporte',
        'Permiso Especial',
    ];

    // Paises
    static COUNTRIES = ['Colombia', 'Estados Unidos'];

    // Valor de fecha en timestamp
    static dateToNumber(data: string | Date | number): number {
        return new Date(data).getTime();
    }

}